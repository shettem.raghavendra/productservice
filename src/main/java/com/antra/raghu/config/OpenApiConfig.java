package com.antra.raghu.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

@Configuration
public class OpenApiConfig {

	@Bean
	public OpenAPI customOpenAPI() {
		return new OpenAPI()
				.info(new Info()
						.title("PRODUCT APPLICATION API")
						.version("3.2GA")
						.description("SAMPLE APP DESC")
						.contact(new Contact()
								.name("RAGHU")
								.email("shettem.raghavendra@antra.com")
								.url("http://antra.com/shettem.raghavendra")
								)
						.termsOfService("http://swagger.io/terms/")
						.license(new License().name("Apache 2.0")
								.url("http://springdoc.org")));
	}
}
