package com.antra.raghu.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.antra.raghu.exception.ProductNotFoundException;

@RestControllerAdvice
public class CustomExceptionHandler {

	@ExceptionHandler(ProductNotFoundException.class)
	public ResponseEntity<String> handleProductNotFound(
			ProductNotFoundException exception
			)
	{
		return new ResponseEntity<String>(
				exception.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
