package com.antra.raghu.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.antra.raghu.entity.Product;
import com.antra.raghu.exception.ProductNotFoundException;
import com.antra.raghu.service.IProductService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/product")
public class ProductRestController {
	
	@Autowired
	private IProductService service;

	//1. create product
	@PostMapping("/create")
	public ResponseEntity<String> createProduct(
			@RequestBody Product product
			) 
	{
		log.info("ENTERED INTO SAVE OPERATION");
		ResponseEntity<String> response = null;
		try {
			Integer id = service.saveProduct(product);
			String message = "Product '"+id+"' created";
			log.debug(message);
			response = ResponseEntity.ok(message);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("PROBLEM IS : {}",e.getMessage());
		}
		log.info("ABOUT TO LEAVE SAVE METHOD");
		return response;
	}
	
	//2. view all products
	@GetMapping("/all")
	public ResponseEntity<List<Product>> getAllProducts() {
		List<Product> list = service.getAllProducts();
		return ResponseEntity.ok(list);
	}
	
	//3. get one product by id
	@GetMapping("/find/{id}")
	public ResponseEntity<Product> getOneProduct(
			@PathVariable Integer id
			)
	{
		log.info("ENTERED INTO FETCH ONE");
		ResponseEntity<Product> response = null;
		try {
			Product p =  service.getOneProduct(id);
			response = ResponseEntity.ok(p);
			log.info("PRODUCT FETCH IS SUCCESS FOR ID {}",id);
		} catch (ProductNotFoundException e) {
			log.error("PROBLEM IS : {}",e.getMessage());
			throw e;
		}
		log.info("ABOUT TO LEAVE THIS FETCH ONE");
		return response;
	}
	
	//4. delete product
	@DeleteMapping("/remove/{id}")
	public ResponseEntity<String> removeProduct(
			@PathVariable Integer id
			) 
	{
		ResponseEntity<String> response = null;
		try {
			service.deleteProduct(id);
			response = ResponseEntity.ok("Product '"+id+"' Removed");
		} catch (ProductNotFoundException e) {
			throw e;
		}
		return response;
	}
	
	//5. update product
	@PutMapping("/update")
	public ResponseEntity<String> updateProduct(
			@RequestBody Product product
			) 
	{
		ResponseEntity<String> response = null;
		try {
			service.updateProduct(product);
			response = ResponseEntity.ok(
					"Product '"+product.getProdId()+"' Updated");
		} catch (ProductNotFoundException e) {
			throw e;
		}
		return response;
	}
	
	
}
 