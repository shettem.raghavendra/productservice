package com.antra.raghu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.antra.raghu.entity.Product;
import com.antra.raghu.exception.ProductNotFoundException;
import com.antra.raghu.repo.ProductRepository;
import com.antra.raghu.service.IProductService;

@Service
public class ProductServiceImpl implements IProductService {
	
	@Autowired
	private ProductRepository repo;
	
	public Integer saveProduct(Product p) {
		return repo.save(p).getProdId();
	}

	
	public void updateProduct(Product p) {
		if(p.getProdId() == null || !repo.existsById(p.getProdId())) {
			throw new ProductNotFoundException("PRODUCT NOT EXIST");
		} else {
			repo.save(p);
		}
	}
	
	public void deleteProduct(Integer id) {
		//repo.deleteById(id);
		repo.delete(getOneProduct(id));
	}

	
	public Product getOneProduct(Integer id) {
		/*Optional<Product> opt =  repo.findById(id);
		if(opt.isPresent()) {
			return opt.get();
		}
		else throw new ProductNotFoundException("PRODUCT NOT EXIST");*/
		return repo.findById(id).orElseThrow(
				()-> new ProductNotFoundException("PRODUCT NOT EXIST"));
		
	}

	public List<Product> getAllProducts() {
		return repo.findAll();
	}

}
