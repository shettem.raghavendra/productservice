package com.antra.raghu.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@Entity
@Table(name="product_tab")
public class Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="prod_id_col")
	private Integer prodId;
	
	@JsonProperty("product_code")
	@Column(name="prod_code_col")
	private String prodCode;
	
	@Column(name="prod_desc_col")
	private String prodDesc;
	
	@Column(name="prod_cost_col")
	private Double prodCost;
	
	@Column(name="prod_img_col")
	private String prodImage;
	
	@Column(name="prod_vendor_col")
	private String prodVendor;
	
	@JsonFormat(shape = Shape.STRING, pattern = "dd-MM-yyyy")
	@Column(name="prod_of_mfg_col")
	private Date dateOfMfg;

}
