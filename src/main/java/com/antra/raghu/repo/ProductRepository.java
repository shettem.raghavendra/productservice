package com.antra.raghu.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.antra.raghu.entity.Product;

public interface ProductRepository 
	extends JpaRepository<Product, Integer>{

}
